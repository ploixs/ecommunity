from abc import ABC, abstractmethod

from buildingenergy import timemg
from ecommunity import indicators
import pytz
import datetime
import enum
import plotly.graph_objs as go
from plotly.subplots import make_subplots


class COLOR(enum.Enum):
    BLINKING_GREEN = 3
    SUPER_GREEN = 2
    GREEN = 1
    WHITE = 0
    RED = -1
    SUPER_RED = -2
    BLINKING_RED = -3


class _HourIterator:
    """Interate the hours while managing the day hour indices."""

    def __init__(self, all_starting_days_hours, last_hours_index: int):
        """Initialize the hour iterator.

        :param all_starting_days_hours: list containing the hour indices of the first hour of every day for the period provided to the ClockStepper
        :type all_starting_days_hours
        :param last_hours_index: last hour index provided to the ClockStepper
        :type last_hours_index: int
        """
        self._all_starting_days_hours = all_starting_days_hours
        self._all_ending_days_hours = []
        for i in range(1, len(self._all_starting_days_hours)):
            self._all_ending_days_hours.append(self._all_starting_days_hours[i] - 1)
        self._all_ending_days_hours.append(last_hours_index)
        self._number_of_days = len(all_starting_days_hours)
        self._current_day_index, self._current_hour_index = 0, 0
        self._day_changed = True
        self._last_hours_index = last_hours_index
        self._init = True

    def next(self):
        """Increment by 1 hour the time.

        :return: true is incrementing is still possible, False otherwise
        :rtype: bool
        """
        if self._current_hour_index >= self._all_ending_days_hours[self._current_day_index]:
            self._day_changed = True
            if self._current_hour_index == self._last_hours_index:
                self._current_hour_index = None
            else:
                self._current_day_index += 1
                self._current_hour_index += 1
        else:
            if self._init:
                self._init = False
            else:
                self._day_changed = False
                self._current_hour_index += 1
        return self._current_hour_index is not None

    @property
    def day_has_changed(self):
        """Return wether incrementing the hour with next() leads to a new day.

        :return: True if new day, False otherwise
        :rtype: bool
        """
        return self._day_changed

    @property
    def number_of_days(self):
        """Return the total number of days that are going to be simulated

        :return: number of days (households must agree
        :rtype: int
        """
        return self._number_of_days

    @property
    def day_hour_indices(self):
        """Return the hour indices corresponding to the current day.

        :return: list of indices
        :rtype
        """
        return [hour_index for hour_index in range(self._all_starting_days_hours[self._current_day_index], self._all_ending_days_hours[self._current_day_index] + 1)]

    @property
    def current_indices(self):
        """Return the index of current day and the one of the current hour too.

        :return: index of current day and the one of the current hour
        :rtype: int, int
        """
        return self._current_day_index, self._current_hour_index

    def reset(self):
        """Reset the time simulation
        """
        self._current_day_index, self._current_hour_index = 0, 0


class Simulator(ABC):
    """
    This abstract class is coordinating "SynchronizedSystems": it's basically a clock with a callback mechanism that daily and hourly call registered SynchronizedSystems. The clock is realistic and for instance, it takes into time shifts twice a year. It's an abstract class that must be subclassed by implementing methods init(), day_update and hour_update.
    """

    def __init__(self, from_local_stringdate: str, to_local_stringdate: str, local_timezone: str = "Europe/Paris", **args):
        """Initialize a simulator that will iterate all the hours between the from_local_stringdate to to_local_stringdate.

        :param from_local_stringdate: starting day for the simulator in format (%d/%m/%M)
        :type from_local_stringdate: str
        :param to_local_stringdate: ending day for the simulator (excluded)in format (%d/%m/%M)
        :type to_local_stringdate: str
        :param local_timezone: local timezone to properly handles string date, defaults to "Europe/Paris"
        :type local_timezone: str, optional
        :param args: arguments transmitted to init()
        """
        self.from_stringdate = from_local_stringdate
        self.to_stringdate = to_local_stringdate
        self.local_timezone = pytz.timezone(local_timezone)
        self.has_result_set = False
        from_utc_datetime = timemg.stringdate_to_datetime(from_local_stringdate, date_format='%d/%m/%Y').astimezone(pytz.utc)
        to_utc_datetime = timemg.stringdate_to_datetime(to_local_stringdate, date_format='%d/%m/%Y').astimezone(pytz.utc)

        utc_datetime = from_utc_datetime  # the time reference is in UTC time
        _former_days_in_year = -1
        all_starting_days_slots = list()
        self.local_datetimes = list()
        k = 0  # the next while-loop computes the indices of each new day of the year
        while to_utc_datetime > utc_datetime:
            local_datetime = self.local_timezone.normalize(utc_datetime.replace(tzinfo=pytz.utc).astimezone(self.local_timezone))
            # UTC time is translated into local time (ie based on local timezone)
            day_in_year = local_datetime.timetuple().tm_yday   # get the day in the year for local tile
            if len(self.local_datetimes) == 0 or day_in_year != _former_days_in_year:  # if a new day is detected...
                all_starting_days_slots.append(k)  # ...store the time index as the time index where a new day has been detected
                _former_days_in_year = day_in_year  #
            self.local_datetimes.append(local_datetime)  # store the local datetime
            utc_datetime += datetime.timedelta(hours=1)  # reference UTC time is increased by 1 hour
            k += 1
        self.hour_iterator = _HourIterator(all_starting_days_slots, k - 1)
        self._day_index, self._current_hour_slot = self.hour_iterator.current_indices
        self._synchronized_systems_groups = dict()

    @property
    def number_of_hours(self):
        """Return the number of hours in the time simulation.

        :return: number of hours
        :rtype: int
        """
        return len(self.local_datetimes)

    @property
    def number_of_days(self):
        """Return the number of days in the time simulation.

        :return: number of hours
        :rtype: int
        """
        return self.hour_iterator.number_of_days

    @property
    def datetimes(self):
        """Return the list of datetimes corresponding to the hours in the time simulation.

        :return: [description]
        :rtype: [type]
        """
        return self.local_datetimes

    def blank_vector(self):
        """Generate a list of zeros whose size is same that the number of hours in the time simulation

        :return: [list of zeros
        :rtype
        """
        return [0 for _ in range(self.number_of_hours)]

    def _register_synchronized_system(self, group_name: str, synchronized_system: 'SynchronizedSystem'):
        """Register a Synchronized System.

        :param group_name: name of the group of systems in which the provided Synchronized System are registered
        :type group_name: str
        :param synchronized_system: A synchronized system to be registered with the simulator: it will receive daily and hourly call back
        :type synchronized_system: AbstractSynchronizedSystem
        """
        if group_name in self._synchronized_systems_groups:
            self._synchronized_systems_groups[group_name].append(synchronized_system)
        else:
            self._synchronized_systems_groups[group_name] = [synchronized_system]

    def get_synchronized_group(self, group_name: str):
        """Return all the Synchronized systems belonging to a same group.

        :param group_name: name of the group of systems in which the provided Synchronized System are registered
        :type group_name: str
        :return: a list of synchronized systems
        :rtype: list[ecommunity.sync.AbstractSynchronizedSystem]
        """
        return self._synchronized_systems_groups[group_name]

    def get_group_names(self):
        """Return the list of groups containing registered synchronized systems.

        :return: a list of groups
        :rtype
        """
        return tuple(self._synchronized_systems_groups.keys())

    def _step_day(self, day_hour_indices, init: bool):
        """Iterate on days by updating Synchronized Systems calling their day_update() method

        :return: the list of hour indices that corresponds to the hour slots of the current day, or None if there is no more day in the simulation.
        :rtype
        """
        the_date = self.datetimes_from_indices(day_hour_indices[0]).date()
        self.day_simulator_step_1(the_date, day_hour_indices, self._synchronized_systems_groups, init)
        for group_name in self._synchronized_systems_groups:
            for synchronized_system in self._synchronized_systems_groups[group_name]:
                synchronized_system.day_system_step_2(day_hour_indices, the_date)
        self.day_simulator_step_3(the_date, day_hour_indices, self._synchronized_systems_groups, init)

    def _step_hour(self, current_hour_index):
        """Iterate on hours by updating Synchronized Systems calling their hour_update() method.

        :return: the current hour slot index within the current day, or None if there is no more hour in the day.
        :rtype
        """
        self.hour_simulator_step_a(self.datetimes_from_indices(current_hour_index), current_hour_index, self._synchronized_systems_groups)
        for group_name in self._synchronized_systems_groups:
            for synchronized_system in self._synchronized_systems_groups[group_name]:
                synchronized_system.hour_system_step_b(current_hour_index, self.datetimes_from_indices(current_hour_index))
        self.hour_simulator_step_c(self.datetimes_from_indices(current_hour_index), current_hour_index, self._synchronized_systems_groups)
        for group_name in self._synchronized_systems_groups:
            for synchronized_system in self._synchronized_systems_groups[group_name]:
                synchronized_system.hour_system_step_d(current_hour_index, self.datetimes_from_indices(current_hour_index))
        self.hour_simulator_step_e(self.datetimes_from_indices(current_hour_index), current_hour_index, self._synchronized_systems_groups)

    @abstractmethod
    def day_simulator_step_1(self, the_date: datetime.date, day_hour_slots, synchronized_systems_groups, init: bool):
        """Perform the computations depicted in this abstract method at the beginning of each day (midnight) i.e. before the households start modify there behavior. It must be subclassed.

        :param the_date: the current date for computations
        :type the_date: datetime.date
        :param day_hour_slots: time slot indice corresponding to the current computed day
        :type day_hour_slots
        :param synchronized_systems_groups: dictionnary of groups of Synchronized Systems
        :type synchronized_systems_groups: dict[str, list[ecommunity.sync.AbstractSynchronizedSystem]]
        :raises NotImplementedError: abstract method to be implemented
        """
        raise NotImplementedError

    @abstractmethod
    def day_simulator_step_3(self, the_date: datetime.date, day_hour_slots, synchronized_systems_groups, init: bool):
        """Perform the computations depicted in this abstract method at the beginning of each day (midnight) i.e. before the households start modify there behavior. It must be subclassed.

        :param the_date: the current date for computations
        :type the_date: datetime.date
        :param day_hour_slots: time slot indice corresponding to the current computed day
        :type day_hour_slots
        :param synchronized_systems_groups: dictionnary of groups of Synchronized Systems
        :type synchronized_systems_groups: dict[str, list[ecommunity.sync.AbstractSynchronizedSystem]]
        :raises NotImplementedError: abstract method to be implemented
        """
        raise NotImplementedError

    @abstractmethod
    def hour_simulator_step_a(self, the_datetime: datetime.datetime, hour_index: int, synchronized_systems_groups):
        """Perform the computations depict in this abstract method at the beginning of each hour of a day. It must be subclassed.

        :param the_datetime: the current date for computations
        :type the_datetime: datetime.datetime
        :param the_hour_slot: time slot index corresponding to the current computed hour
        :type the_hour_slot: int
        :param synchronized_systems_groups: dictionnary of groups of Synchronized Systems
        :type synchronized_systems_groups: dict[str, list[AbstractSynchronizedSystem]]
        :raises NotImplementedError: abstract method to be implemented
        """
        raise NotImplementedError

    @abstractmethod
    def hour_simulator_step_c(self, the_datetime: datetime.datetime, hour_index: int, synchronized_systems_groups):
        """Perform the computations depict in this abstract method at the beginning of each hour of a day. It must be subclassed.

        :param the_datetime: the current date for computations
        :type the_datetime: datetime.datetime
        :param the_hour_slot: time slot index corresponding to the current computed hour
        :type the_hour_slot: int
        :param synchronized_systems_groups: dictionnary of groups of Synchronized Systems
        :type synchronized_systems_groups: dict[str, list[AbstractSynchronizedSystem]]
        :raises NotImplementedError: abstract method to be implemented
        """
        raise NotImplementedError

    @abstractmethod
    def hour_simulator_step_e(self, the_datetime: datetime.datetime, hour_index: int, synchronized_systems_groups):
        """Perform the computations depict in this abstract method at the end of each hour of a day. It must be subclassed.

        :param the_datetime: the current date for computations
        :type the_datetime: datetime.datetime
        :param the_hour_slot: time slot index corresponding to the current computed hour
        :type the_hour_slot: int
        :param synchronized_systems_groups: dictionnary of groups of Synchronized Systems
        :type synchronized_systems_groups: dict[str, list[AbstractSynchronizedSystem]]
        :raises NotImplementedError: abstract method to be implemented
        """
        raise NotImplementedError

    def datetimes_from_indices(self, day_hours_indices):
        """Convert a list or a single hour slot indices, corresponding to the hour slots counted from the beginning of the simulation, into a list of, or a single, datetime(s).

        :param day_hours_indices: a list or a single hour slot indices
        :type day_hours_indices
        :return: a list of, or a single, datetime(s)
        :rtype: datetime.datetime or list[datetime.datetime]
        """
        if type(day_hours_indices) == int:
            return self.local_datetimes[day_hours_indices]
        else:
            return [self.local_datetimes[k] for k in day_hours_indices]

    def run(self):
        """Start a simulation."""
        init = True
        while self.hour_iterator.next():
            _, current_hour_index = self.hour_iterator.current_indices
            if self.hour_iterator.day_has_changed:
                self._step_day(self.hour_iterator.day_hour_indices, init)
                init = False
            self._step_hour(current_hour_index)

        self.finalize(self._synchronized_systems_groups)

        self.actual_community_supplied_powers = self.blank_vector()
        self.predicted_community_supplied_powers = self.blank_vector()
        self.actual_community_consumed_powers = self.blank_vector()
        self.predicted_community_consumed_powers = self.blank_vector()
        for pv_plant in self._synchronized_systems_groups['PVplant']:
            for i in range(self.number_of_hours):
                self.actual_community_supplied_powers[i] += pv_plant.actual_supplied_powers[i]
                self.predicted_community_supplied_powers[i] += pv_plant.predicted_supplied_powers[i]
        #total_contribution = 0
        for house in self._synchronized_systems_groups['house']:
            for i in range(self.number_of_hours):
                self.actual_community_consumed_powers[i] += house.actual_consumed_powers[i]
                self.predicted_community_consumed_powers[i] += house.predicted_consumed_powers[i]
            house._set_results(self.actual_community_supplied_powers, self.actual_community_consumed_powers,  self.hour_colors)
            #total_contribution += house.contribution
        for house in self._synchronized_systems_groups['house']:
            house.actual_share = indicators.member_contribution(self, house, self.actual_community_supplied_powers, self.actual_community_consumed_powers, self.hour_colors)
            #house.actual_share = house.contribution / total_contribution
            print(house)
        self.has_result_set = True
        print(self)
        print()
        self.plot_results()

    @abstractmethod
    def finalize(self, synchronized_systems_groups):
        """Executed at the end of the simulation. Abstract method to be subclassed.

        :param synchronized_systems_groups: list of synchronized systems
        :type synchronized_systems_groups: list[str, dict[str,AbstractSynchronizedSystem]]
        :raises NotImplementedError: must be subclassed
        """
        raise NotImplementedError

    def __str__(self) -> str:
        """Describe the simulator.

        :return: description
        :rtype: str
        """
        if not self.has_result_set:
            string = '- Clock time in [%s, %s] with synchronized systems: \n' % (self.from_stringdate, self.to_stringdate)
            for group_name in self._synchronized_systems_groups:
                string += '* group: %s\n' % group_name
                for synchronized_system in self._synchronized_systems_groups[group_name]:
                    string += ' - ' + synchronized_system.name + '\n'
            if self._current_hour_slot is not None:
                '\tcurrent hour is %s\n' % (timemg.datetime_to_stringdate(self.datetimes_from_indices(self._current_hour_slot)))
        else:
            string = '# community\n'
            string += 'alert threshold: %.0fW\n' % self.no_alert_threshold
            string += 'self-consumption: %.0f%% > %.0f%%\n' % (100 * self.predicted_self_consumption, 100 * self.actual_self_consumption)
            string += 'dependency: %.0f%% > %.0f%%\n' % (100 * self.predicted_dependency, 100 * self.actual_dependency)
            string += 'autonomy: %.0f%% > %.0f%%\n' % (100 * self.predicted_autonomy, 100 * self.actual_autonomy)
            string += 'cost: %.0f€ > %.0f€\n' % (self.predicted_cost, self.actual_cost)
            string += 'NEEG per day: %.0fkWh/day > %.0fkWh/day\n' % (self.predicted_NEEG_per_day, self.actual_NEEG_per_day)
            string += 'renunciation: %.0f%%\n' % (100 * self.renunciation)
            string += 'effort: %.0f%%\n' % (100 * self.effort)
            string += 'daily savings per house: %.2f€\n' % self.daily_savings_per_member
            string += 'alerts/day: %.0f\n' % (self.alerts_per_day)
        return string

    @property
    def predicted_self_consumption(self):
        return indicators.self_consumption(self.predicted_community_consumed_powers, self.predicted_community_supplied_powers)

    @property
    def actual_self_consumption(self):
        return indicators.self_consumption(self.actual_community_consumed_powers, self.actual_community_supplied_powers)

    @property
    def predicted_dependency(self):
        return indicators.dependency(self.predicted_community_consumed_powers, self.predicted_community_supplied_powers)

    @property
    def actual_dependency(self):
        return indicators.dependency(self.actual_community_consumed_powers, self.actual_community_supplied_powers)

    @property
    def predicted_autonomy(self):
        return indicators.autonomy(self.predicted_community_consumed_powers, self.predicted_community_supplied_powers)

    @property
    def actual_autonomy(self):
        return indicators.autonomy(self.actual_community_consumed_powers, self.actual_community_supplied_powers)

    @property
    def predicted_cost(self):
        return indicators.cost(self.predicted_community_consumed_powers, self.predicted_community_supplied_powers)

    @property
    def actual_cost(self):
        return indicators.cost(self.actual_community_consumed_powers, self.actual_community_supplied_powers)

    @property
    def predicted_NEEG_per_day(self):
        return indicators.NEEG_per_day(self.predicted_community_consumed_powers, self.predicted_community_supplied_powers)

    @property
    def actual_NEEG_per_day(self):
        return indicators.NEEG_per_day(self.actual_community_consumed_powers, self.actual_community_supplied_powers)

    @property
    def renunciation(self):
        return indicators.renunciation(self.predicted_community_consumed_powers, self.actual_community_consumed_powers)

    @property
    def effort(self):
        return indicators.effort(self.predicted_community_consumed_powers, self.actual_community_consumed_powers)

    @property
    def daily_savings_per_member(self):
        return (indicators.cost(self.predicted_community_consumed_powers, self.predicted_community_supplied_powers) - indicators.cost(self.actual_community_consumed_powers, self.actual_community_supplied_powers)) / self.number_of_days / len(self.houses)

    @property
    def alerts_per_day(self):
        alerts = self.blank_vector()
        number_of_members = len(self._synchronized_systems_groups['house'])
        for i in range(len(alerts)):
            for house in self._synchronized_systems_groups['house']:
                if house.hour_colors[i] == COLOR.RED or house.hour_colors[i] == COLOR.GREEN:
                    alerts[i] += 1
                elif house.hour_colors[i] == COLOR.SUPER_RED or house.hour_colors[i] == COLOR.SUPER_GREEN:
                    alerts[i] += 1.5
                elif house.hour_colors[i] == COLOR.BLINKING_RED or house.hour_colors[i] == COLOR.BLINKING_GREEN:
                    alerts[i] += 2
            alerts[i] = alerts[i] / number_of_members
        return sum(alerts) / self.number_of_days

    def plot_results(self):
        fig = make_subplots(rows=2, cols=1, shared_xaxes=True)
        fig.add_trace(go.Scatter(x=self.datetimes, y=self.predicted_community_supplied_powers, name='predicted supplied powers'), row=1, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=self.actual_community_supplied_powers, name='actual supplied powers'), row=1, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=self.predicted_community_consumed_powers, name='predicted consumed powers'), row=1, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=self.actual_community_consumed_powers, name='actual consumed powers'), row=1, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=[-1 if c == COLOR.RED else None for c in self.hour_colors], mode='markers', marker_color='red', marker_size=20, showlegend=False), row=2, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=[-2 if c == COLOR.SUPER_RED else None for c in self.hour_colors], mode='markers', marker_color='darkred', marker_size=20, showlegend=False), row=2, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=[-3 if c == COLOR.BLINKING_RED else None for c in self.hour_colors], mode='markers', marker_color='darkred', marker_size=20, showlegend=False), row=2, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=[1 if c == COLOR.GREEN else None for c in self.hour_colors], mode='markers', marker_color='green', marker_size=20, showlegend=False), row=2, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=[2 if c == COLOR.SUPER_GREEN else None for c in self.hour_colors], mode='markers', marker_color='darkgreen', marker_size=20, showlegend=False), row=2, col=1)
        fig.add_trace(go.Scatter(x=self.datetimes, y=[3 if c == COLOR.BLINKING_GREEN else None for c in self.hour_colors], mode='markers', marker_color='darkgreen', marker_size=20, showlegend=False), row=2, col=1)
        fig.show()



class SynchronizedSystem(ABC):
    """
    An abstract synchronized system is a system that is added into a simulation. Different methods have to be implemented: init() for specific initialization, day_update() for daily updates and hour_update() for hourly updates.
    """

    def __init__(self, simulator: Simulator, name: str):
        """Initialize a Synchronized System

        :param simulator: the simulator
        :type simulator: AbstractSimulator
        :param name: name of the synchronized system composed like "group_name:system_name"
        :type name: str
        """
        self.simulator = simulator
        self.group_name, self.name = name.split(':')
        simulator._register_synchronized_system(self.group_name, self)
        self.current_day_hours_indices = None
        self.day_ahead_hours_indices = None

    @abstractmethod
    def day_system_step_2(self, hour_slots, the_date: datetime.date):
        """Call for each day update. Must be subclassed.

        :param hour_slots: hour indices corresponding to the current day
        :type hour_slots
        :param the_date: date of the current day
        :type the_date: datetime.date
        :raises NotImplementedError: Must be subclassed.
        """
        raise NotImplementedError

    @abstractmethod
    def hour_system_step_b(self, hour_slot: int, the_datetime: datetime.datetime):
        """Call for each hour update. Must be subclassed.

        :param hour_slot: current hour index
        :type hour_slot: int
        :param the_datetime: datetime of the current hour
        :type the_datetime: datetime.datetime
        :raises NotImplementedError: Must be subclassed.
        """
        raise NotImplementedError

    @abstractmethod
    def hour_system_step_d(self, hour_slot: int, the_datetime: datetime.datetime):
        """Call for each hour update. Must be subclassed.

        :param hour_slot: current hour index
        :type hour_slot: int
        :param the_datetime: datetime of the current hour
        :type the_datetime: datetime.datetime
        :raises NotImplementedError: Must be subclassed.
        """
        raise NotImplementedError
