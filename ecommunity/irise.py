from __future__ import annotations
from fileinput import filename
from ecommunity import scheduler, indicators
from buildingenergy import timemg
import sqlalchemy
import numpy
from buildingenergy import openweather
from buildingenergy import solar
import prettytable
import random
import configparser


def filter_outliers(values: list, outlier_sensitivity: float = 3, min_value: float = None, max_value: float = None) -> None:
    """Remove outliers. Outliers are defined as 2 opposite important derivatives in consecutive values (possibly with different time deltas) or as values out of a given range. Function has no output but replace each detected outlier but the average value of the left and right neighbors in the 'values' vector provided as input.
    :param variable_name: name of the variable used to display info when an outlier is detected
    :param values: list of values
    :param outlier_sensitivity: detection sensitivity to detection of 2 consecutive opposite important derivatives; it defines a threshold with outlier_sensitivity * standard deviation of delta values
    :param min_value: minimum data value for acceptable range
    :param max_value: maximum data value for acceptable range
    """
    delta_values = [values[k + 1] - values[k] for k in range(len(values) - 1)]
    mean_delta = numpy.mean(delta_values)
    sigma_delta = numpy.std(delta_values)
    for k in range(1, len(values) - 1):
        backward_diff = values[k] - values[k - 1]
        forward_diff = values[k + 1] - values[k]
        if (abs(backward_diff) > mean_delta + outlier_sensitivity * sigma_delta) and (abs(forward_diff) > mean_delta + outlier_sensitivity * sigma_delta) and backward_diff * forward_diff < 0:
            values[k] = (values[k - 1] + values[k + 1]) / 2
        if (max_value is not None) and (values[k] > max_value):
            values[k] = values[k - 1]
        elif (min_value is not None) and (values[k] < min_value):
            values[k] = values[k - 1]
    return values


class IRISE:
    """House data extractor from the IRISE SQLite3 database: it contains all the electric consumptions measured for each of the 100 houses involved in the REMODECE/IRISE project during the beginning of 1998 till 2000. Because the weather data are going from 2015 to 2020, the data are shifted with 17 years in order for the day of weeks to be preserved. The administrative time changes carried you twice a year are taken into accout."""

    def __init__(self, simulator: scheduler.AbstractSimulator, randomize_households=False):
        """Initialize the house data extractor by getting connected to the database, extracting all the data and closing the connection.

        :param simulator: the simulator that is going to use the IRISE database
        :type from_datestring: scheduler.IRISEsimulator
        """
        config = configparser.ConfigParser()
        config.read("config.ini")
        filename = config['database']['irise']
        database = sqlalchemy.create_engine('sqlite:///'+filename)
        database_connection = database.connect()
        self.irise_houses = list()
        query = database_connection.execute("SELECT ID, ZIPcode, Location  FROM HOUSE WHERE ZIPcode LIKE '381%'")
        rows = query.cursor.fetchall()
        number_of_houses = len(rows)
        for id, zip_code, location in rows:
            self.irise_houses.append(IRISEhouse(database_connection, id, zip_code, location, simulator, 1/number_of_houses, randomize_households))
        database_connection.invalidate()
        database.dispose()


class IRISEhouse:
    """A house generated from the IRISE SQLite3 database."""

    def __init__(self, database_connection, house_id: int, zip_code: str, location: str, simulator: scheduler.Simulator, share: float, randomize_households):
        """Initialize a house

        :param database_connection: SQLalchemy database connection
        :type database_connection: SQLalchemy
        :param id: id of the house
        :type id: int
        :param zip_code: zip code of the house
        :type zip_code: str
        :param location: location of the house
        :type location: str
        :param simulator: the simulator that is using the IRISE database
        :type from_datestring: scheduler.IRISEsimulator
        :param share: theoretical contribution in the community
        :type share: float
        """
        self.name = 'house:id=%i@%s-%s' % (house_id, zip_code, location)
        self.id_appliances = dict()
        self.share = share
        self.randomize_households = randomize_households

        query = database_connection.execute("SELECT ID, Name FROM Appliance WHERE HouseIDREF=%i" % house_id)
        for appliance_id, name in query.cursor.fetchall():
            self.id_appliances[appliance_id] = Appliance(database_connection, appliance_id, house_id, name, simulator)
            if self.id_appliances[appliance_id].name == 'site_consumption':
                self._site_consumptions = filter_outliers(self.id_appliances[appliance_id].values, outlier_sensitivity=3)
            else:
                print(self.id_appliances[appliance_id].name, end=', ')
        print()

    def site_consumptions(self):
        """Return the site consumption of the house.

        :return: site consumption (detailed consumptions are also available)
        :rtype: list[float]
        """
        return self._site_consumptions

    def __str__(self) -> str:
        """Return a description of the house

        :return: text description
        :rtype: str
        """
        return 'House %i located at %i %s' % (self.id, self.zip_code, self.location)


class Appliance:
    """An appliance related to a house."""

    def __init__(self, database_connection, id: int, house_id: int, name: str, simulator: scheduler.Simulator):
        """Initialize the appliance.

        :param database_connection: sqlite3 database connection
        :type database_connection: SQLalchemy
        :param id: identifier of the appliance in a house
        :type id: int
        :param house_id: identifier of the house containing the appliance
        :type house_id: int
        :param name: name of the appliance
        :type name: str
        :param simulator: the simulator that is using the IRISE database
        :type from_datestring: scheduler.IRISEsimulator
        """
        self.name = name.split(" (")[0]
        self.name = self.name.replace(" ", "_")
        self.name = self.name.lower()

        from_irise_datestring = timemg.stringdate_shift_year(simulator.from_stringdate + ' 0:00:00', -17)
        to_irise_datestring = timemg.stringdate_shift_year(simulator.to_stringdate + ' 0:00:00', -17)
        request = "SELECT EpochTime, Value FROM Consumption WHERE HouseIDREF=%i AND ApplianceIDREF=%i AND EpochTime >= %i AND EpochTime < %i ORDER BY EpochTime ASC" % (house_id, id, timemg.stringdate_to_epochtimems(from_irise_datestring) / 1000, timemg.stringdate_to_epochtimems(to_irise_datestring) / 1000)
        query = database_connection.execute(request)
        epochtime_values = query.cursor.fetchall()
        epochtime_values_dict = dict()
        for i in range(0, len(epochtime_values), 6):
            epochtime_values_dict[timemg.datetime_shift_year(timemg.epochtimems_to_datetime(epochtime_values[i][0] * 1000), 17)] = sum([epochtime_values[i + j][1] for j in range(0, 6)])

        self.values = list()
        previous_naive_simulator_datetime = None
        for dt in simulator.datetimes:
            naive_simulator_datetime = dt.replace(tzinfo=None)
            if naive_simulator_datetime in epochtime_values_dict:
                self.values.append(epochtime_values_dict[naive_simulator_datetime])
            else:
                self.values.append(epochtime_values_dict[previous_naive_simulator_datetime])
            previous_naive_simulator_datetime = naive_simulator_datetime

    def __str__(self) -> str:
        """Return a description of the appliance.

        :return: a text description
        :rtype: str
        """
        return 'Appliance: ' + self.name


class PVplant(scheduler.SynchronizedSystem):
    """A synchronized system representing a PV plant. Prediction of production is dependent on the weather but ignore nebulosity. AAdditional and random variation is introduced. The actual is the one calculated exactly with the weather and the nebulosity.
    """
    def __init__(self, simulator, name, pv_efficiency=.13, pv_surface=88, slope=147, exposure=2, randomize_ratio=.2):
        """Initialize a photovoltaic plant owned by the energy community.

        :param simulator: the simulator of the community behavior
        :type simulator: scheduler.IRISEsimulator
        :param name: name of the PVplant preceded by it's group, here "PVplant:plan_name"
        :type name: str
        :param pv_efficiency: energy conversion efficiency for the PV, defaults to .13
        :type pv_efficiency: float, optional
        :param pv_surface: surface of the PV panels, defaults to 50
        :type pv_surface: int, optional
        :param randomize_ratio: amplitude of the random variation added to the production, defaults is to .2
        :type randomize_ratio: float, optional
        """
        super().__init__(simulator, name)

        self.site_weather_data = openweather.OpenWeatherMapJsonReader('grenoble_weather2015-2019.json', from_stringdate=simulator.from_stringdate + ' 0:00:00', to_stringdate=simulator.to_stringdate + ' 0:00:00', sea_level_in_meter=330, albedo=.1).site_weather_data

        solar_model = solar.SolarModel(site_weather_data=self.site_weather_data)
        phisun_predicted = solar_model.solar_irradiations(slope_in_deg=slope-2, exposure_in_deg=exposure-2)['total']
        phisun_actual = solar_model.solar_irradiations(slope_in_deg=slope, exposure_in_deg=exposure, ignore_nebulosity=False)['total']
        for i in range(len(phisun_actual)):
            phisun_actual[i] = phisun_actual[i] * random.uniform(1 - randomize_ratio, 1 + randomize_ratio)

        self.actual_supplied_powers = [P * pv_efficiency * pv_surface for P in phisun_actual]
        self.predicted_supplied_powers = [P * pv_efficiency * pv_surface for P in phisun_predicted]


class House(scheduler.SynchronizedSystem):
    """House corresponding to a house described in the IRISE database."""

    def __init__(self, simulator: scheduler.AbstractSimulator, name: str, irise_house: IRISEhouse):
        super().__init__(simulator, name)
        self.predicted_consumed_powers = irise_house.site_consumptions()
        self.actual_consumed_powers = []
        self.theoretical_share = irise_house.share
        self.actual_share = None
        self.randomize_households = irise_house.randomize_households
        self.hour_colors = None
        self.actual_communities_supplied_powers = None
        self.actual_community_consumptions = None
        self.has_result_set = False

    def _set_results(self, actual_communities_supplied_powers, actual_community_consumptions,  hour_colors):
        self.hour_colors = hour_colors
        self.actual_communities_supplied_powers = actual_communities_supplied_powers
        self.actual_community_consumptions = actual_community_consumptions
        self.has_result_set = True

    @property
    def contribution(self):
        if self.has_result_set:
            _contribution = 0
            for i in range(len(self.actual_consumed_powers)):
                member_contribution = self.actual_consumed_powers[i] - self.predicted_consumed_powers[i]
                theoretical_contribution = self.theoretical_share * (self.actual_communities_supplied_powers[i] - self.actual_community_consumptions[i])
                _contribution += abs(member_contribution - theoretical_contribution)
            return _contribution / len(self.actual_consumed_powers)
        else:
            raise Exception("Results have not been set")

    def __str__(self):
        string = '# house %s\n' % self.name
        if self.has_result_set:
            _, color_level_average_values = indicators.color_statistics(self.actual_consumed_powers, self.predicted_consumed_powers, self.hour_colors)
            ptable = prettytable.PrettyTable(['color','average power variation'])
            for color in color_level_average_values:
                ptable.add_row([scheduler.COLOR(color), color_level_average_values[color]])
            string += ptable.__str__() + '\n'
            string += 'Actual share is %.0f%% for a theoretical share of %.0f%%' % (self.actual_share * 100, self.theoretical_share * 100)
        return string
