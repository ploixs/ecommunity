import numpy
import buildingenergy
from ecommunity import scheduler
import random

def self_consumption(consumption, production):
    """Compute the ratio of production that is consumed locally

    :param consumption: series of consumption values
    :type consumption
    :param production: series of production values
    :type production
    :return: self-consumption coefficient
    :rtype: float
    """
    locally_consumed_production = 0
    for i in range(len(consumption)):
        locally_consumed_production += min(production[i], consumption[i])
    return locally_consumed_production / sum(production)


def self_sufficiency(consumption, production):
    """Compute the ratio of consumption that is produced locally

    :param consumption: series of consumption values
    :type consumption
    :param production: series of production values
    :type production
    :return: self-production coefficient
    :rtype: float
    """
    locally_fed_consumption = 0
    for i in range(len(consumption)):
        if consumption[i] == 0 or production[i] > consumption[i]:
            locally_fed_consumption += consumption[i]
        elif production[i] <= consumption[i]:
            locally_fed_consumption += production[i]
    return locally_fed_consumption / sum(consumption)


def dependency(consumption, production):
    """Compute the ratio of consumption that is coming from the grid

    :param consumption: series of consumption values
    :type consumption
    :param production: series of production values
    :type production
    :return: self-production coefficient
    :rtype: float
    """
    return 1 - self_sufficiency(consumption, production)


def individual_contribution(theoretical_contribution_ratio, actual_individual_consumptions, predicted_individual_consumptions, actual_community_consumptions, actual_communities_supplied_powers):
    _contribution = 0
    for i in range(len(actual_individual_consumptions)):
        member_contribution = actual_individual_consumptions[i] - predicted_individual_consumptions[i]
        theoretical_contribution = theoretical_contribution_ratio * (actual_communities_supplied_powers[i] - actual_community_consumptions[i])
        _contribution += abs(member_contribution - theoretical_contribution)
    return _contribution / len(actual_individual_consumptions)


def autonomy(consumption, production):
    """Compute the ratio of autonomy wrt the grid

    :param consumption: series of consumption values
    :type consumption
    :param production: series of production values
    :type production
    :return: autonomy coefficient
    :rtype: float
    """
    total_consumption = 0
    total_production = 0

    for i in range(len(consumption)):
        total_consumption += consumption[i]
        total_production += production[i]
    if total_production == 0:
        return 1
    if total_consumption == 0:
        return None
    else:
        return total_production / total_consumption


def renunciation(consumption_before, consumption_after):
    """Quantity of renounced power

    :param consumption_before: expected consumption without incitation
    :type consumption_before
    :param consumption_after: expected consumption without incitation
    :type consumption_after
    :return: coefficient between 0 (no renunciation) to infinity
    :rtype: float
    """
    return max(0, sum(consumption_before) - sum(consumption_after)) / sum(consumption_before)


def effort(consumption_before, consumption_after, threshold:float = .2):
    """effort is the ratio of time where the consumption is significantly changed, accroding to the threshold

    :param consumption_before: expected consumption without incitation
    :type consumption_before
    :param consumption_after: expected consumption without incitation
    :type consumption_after
    :param threshold: variation in the consumption below which it's consider that change is not annoying for house inhabitants
    :type threshold: float
    :return: coefficient between 0 (no renunciation) to infinity
    :rtype: float
    """
    _effort = 0
    for i in range(len(consumption_before)):
        _effort += ( abs(consumption_before[i] - consumption_after[i])) > (threshold * consumption_after[i])
    return _effort / len(consumption_before)


def cost(consumption, production, grid_injection_tariff_kWh:float = .1, grid_drawn_tariff_kwh: float = .2):
    """energy cost of the provided sequences of consumptions and production values.

    :param consumption: power consumption values
    :type consumption
    :param production: power production values
    :type production
    :param grid_injection_tariff_kWh: tariff for 1kWh of electricity sold to the grid
    :type grid_injection_tariff_kWh: float
    :param grid_drawn_tariff_kWh: tariff for 1kWh of electricity bought on the grid
    :type grid_injection_tariff_kWh: float
    :return: a cost in euros
    :rtype: float
    """
    _cost = 0
    for i in range(len(consumption)):
        if consumption[i] > production[i]:
            _cost += (consumption[i] - production[i]) / 1000 * grid_drawn_tariff_kwh
        else:
            _cost += (consumption[i] - production[i]) / 1000 * grid_injection_tariff_kWh
    return _cost


def NEEG_per_day(consumption, production):
    """net energy exchanged with the grid per day in kWh

    :param consumption: power consumption values
    :type consumption
    :param production: power production values
    :type production
    :return: a quantity of energy per day in kWh
    :rtype: float
    """
    return sum([abs(consumption[i] - production[i]) for i in range(len(consumption))]) * 24 / len(consumption) / 1000


def member_contribution(self, house, actual_community_supplied_powers, actual_community_consumed_powers, hour_colors):
    _member_contribution = 0
    _community_contribution = 0
    for h in range(self.number_of_hours):
        coef = int(hour_colors[h].value > 0) - int(hour_colors[h].value < 0)
        _member_contribution += coef * (house.theoretical_share * actual_community_supplied_powers[h] - house.actual_consumed_powers[h])
        _community_contribution += coef * (actual_community_supplied_powers[h] - actual_community_consumed_powers[h])
    if _community_contribution == 0:
        return 0
    else:
        return _member_contribution / _community_contribution


def color_statistics(actual_consumed_powers, predicted_consumed_powers, hour_colors):
    color_level_values = dict()
    for i in range(len(hour_colors)):
        if hour_colors[i] not in color_level_values:
            color_level_values[hour_colors[i]] = list()
        color_level_values[hour_colors[i]].append(actual_consumed_powers[i] - predicted_consumed_powers[i])
    color_level_average_values = dict()
    color_level_ratio = dict()
    for color in color_level_values:
        if len(color_level_values[color]) == 0:
            color_level_average_values[color] = None
            color_level_ratio[color] = 0
        else:
            color_level_average_values[color] = sum(color_level_values[color]) / len(color_level_values[color])
            color_level_ratio[color] = len(color_level_values[color]) / len(hour_colors)
    return color_level_ratio, color_level_average_values


class ColorAdapter:

    def __init__(self, colors, color_ratios, supplied_powers, consumed_powers):
        delta_powers = [supplied_powers[i] - consumed_powers[i] for i in range(len(consumed_powers))]
        delta_powers.extend([-d for d in delta_powers[::-1]])
        delta_powers.sort()
        number_of_delta_powers_per_color = [int(color_ratio * len(delta_powers)) for color_ratio in color_ratios]
        number_of_delta_powers_per_color[int((2 * len(colors) - 1) / 2)] += len(delta_powers) - sum(number_of_delta_powers_per_color)  # add missing values, due to rounding, to central color
        self.colors = colors
        self.color_index_deltas_powers = {i:[] for i in range(len(colors))}
        color_index = 0
        self.color_index_deltas_powers[0] = list()
        for i in range(len(delta_powers)):
            if len(self.color_index_deltas_powers[color_index]) < number_of_delta_powers_per_color[color_index]:
                self.color_index_deltas_powers[color_index].append(delta_powers[i])
            else:
                color_index += 1
                self.color_index_deltas_powers[color_index].append(delta_powers[i])

    def get_color(self, supplied_power, consumed_power):
        delta_power = supplied_power - consumed_power
        returned_color = scheduler.COLOR.WHITE
        if len(self.color_index_deltas_powers[0])>0 and delta_power < self.color_index_deltas_powers[0][-1]:
            returned_color =  self.colors[0]
        elif len(self.color_index_deltas_powers[len(self.colors)-1])>0 and delta_power > self.color_index_deltas_powers[len(self.colors)-1][0]:
            returned_color =  self.colors[-1]
        else:
            for i in range(1, len(self.colors)-1, 1):
                if len(self.color_index_deltas_powers[i])>0 and self.color_index_deltas_powers[i][0] <= delta_power <= self.color_index_deltas_powers[i][-1]:
                    returned_color =  self.colors[i]
        if returned_color is None:
            return scheduler.COLOR.WHITE
        else:
            return returned_color


if __name__ == '__main__':
    colors = (scheduler.COLOR.BLINKING_RED, scheduler.COLOR.SUPER_RED, scheduler.COLOR.RED, scheduler.COLOR.WHITE, scheduler.COLOR.GREEN, scheduler.COLOR.SUPER_GREEN, scheduler.COLOR.BLINKING_GREEN)
    color_ratios = (.01, .04, .2, .5, .2, .04, .01)
    n=7*24
    consumed_powers = [random.uniform(0,2000) for _ in range(n)]
    supplied_powers = [random.uniform(0,10000) for _ in range(n)]
    color_adapter = ColorAdapter(colors, color_ratios, supplied_powers, consumed_powers)
    print('white', color_adapter.get_color(5000, 5000))
    print('red', color_adapter.get_color(0, 5000))
    print('green', color_adapter.get_color(5000, 0))
    print('superred', color_adapter.get_color(0, 8700))
    print('supergreen', color_adapter.get_color(8700, 0))
    print('blinkingred', color_adapter.get_color(0, 9600))
    print('blinkinggreen', color_adapter.get_color(9600, 0))