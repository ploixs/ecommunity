import flask, flask_restful
import json

app = flask.Flask(__name__)  # web application
api = flask_restful.Api(app)  # RESTful extension


class Test(flask_restful.Resource):

    members = {1: ('Jean', 'Dupont'), 2: ('Jacques', 'Olivier'), 3: ('Corinne', 'François')}

    def get(self, request, id, encod):  # implement an http get service with 2 parameters
        if request == 'member':
            if encod == 'xml':  # contain the response in XML format
                response = flask.Response('<?xml version="1.0"?><name given="{0}" family="{1}"/>'.format(Test.members[int(id)][0], Test.members[int(id)][1]), mimetype='text/xml')
            else:  # contain the response in json format
                response = flask.Response(json.dumps(Test.members[int(id)]), mimetype='text/json')
            response.status_code = 200  # add code 200, which means ok ie request understood
        return response


api.add_resource(Test, '/info/<request>/<id>/<encod>')  # class Test will manage the service 'info' with 2 parameters
app.run(port=8080, debug=True)  # server is launched on port 1080 of local machine

#Test with a web browser:
#        http://localhost:5000/info/member/3/xml (or json)