import requests

server_port = 8080
response = requests.get(url="http://127.0.0.1:%i/info/member/2/xml" % server_port)
print('status_code:', response.status_code)
print('content-type:', response.headers['content-type'])
print('text:', response.text)

response = requests.get(url="http://127.0.0.1:%i/info/member/2/json" % server_port)
print('status_code:', response.status_code)
print('content-type:', response.headers['content-type'])
print('text:', response.text)