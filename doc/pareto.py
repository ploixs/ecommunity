import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import numpy as np

results = {}
# BASIC
# Predicted self-consumption: 75%, dependency: 73%, autonomy: 36%, cost: 2229€, neeg: 38kWh/day
# Actual self-consumption: 81%, dependency: 66%, autonomy: 36%, cost: 1743€, neeg: 29kWh/day, renunciation: 14%, effort: 40%
# daily savings per house= 0.28€

results['BASIC'] = (.14, .4, .28)  # renun, effort, savings

# RED_PRIORITY
# Predicted self-consumption: 75%, dependency: 73%, autonomy: 36%, cost: 2229€, neeg: 38kWh/day
# Actual self-consumption: 73%, dependency: 62%, autonomy: 36%, cost: 1193€, neeg: 24kWh/day, renunciation: 32%, effort: 52%
# daily savings per house= 0.59€
results['RED_PRIORITY'] = (.32, .52, .59)  # renun, effort, savings

# REACTIVE
# Predicted self-consumption: 75%, dependency: 73%, autonomy: 36%, cost: 2229€, neeg: 38kWh/day
# Actual self-consumption: 88%, dependency: 60%, autonomy: 36%, cost: 1449€, neeg: 24kWh/day, renunciation: 22%, effort: 50%
# daily savings per house= 0.45€
results['REACTIVE'] = (.22, .5, .45)  # renun, effort, savings

# ADAPT
# Predicted self-consumption: 75%, dependency: 73%, autonomy: 36%, cost: 2229€, neeg: 38kWh/day
# Actual self-consumption: 79%, dependency: 67%, autonomy: 36%, cost: 1692€, neeg: 29kWh/day, renunciation: 16%, effort: 39%
# daily savings per house= 0.31€
results['REACTIVE'] = (.16, .39, .31)  # renun, effort, savings

# REACTADAPT
# Predicted self-consumption: 75%, dependency: 73%, autonomy: 36%, cost: 2229€, neeg: 38kWh/day
# Actual self-consumption: 86%, dependency: 59%, autonomy: 36%, cost: 1357€, neeg: 23kWh/day, renunciation: 25%, effort: 50%
# daily savings per house= 0.50€
results['REACTADAPT'] = (.25, .50, .5)  # renun, effort, savings

results['COACHING'] = (.39, .59, 0.71)

# Actual self-consumption: 74%, dependency: 74%, autonomy: 36%, cost: 2229€, neeg: 38kWh/day, renunciation: 0%, effort: 0%
# daily savings per house= 0.00€
results['NOCONTROL'] = (0, 0, 0)

fig = plt.figure(figsize=(12, 12))
ax = fig.add_subplot(projection='3d')
legend = []
xs = []
ys = []
zs = []
for algo in results:
    legend.append(algo)
    x, y, z = results[algo]
    xs.append(x)
    ys.append(y)
    zs.append(z)
    ax.text(x, y, z, algo)

ax.set_xlabel('renunciation', fontweight ='bold')
ax.set_ylabel('effort', fontweight ='bold')
ax.set_zlabel('cost', fontweight ='bold')
ax.scatter(xs, ys, zs)
plt.show()
