import uvicorn  # server
from fastapi import FastAPI, HTTPException, status # Restful service
from typing import Tuple, List, Optional  # use to declate complex data type
from pydantic import BaseModel  # enforce data type validation


class MemberSchema(BaseModel):  # schema of the data type Member
    name: str
    share: float
    address: Optional[str] = None


class Member:

    def __init__(self, member_schema: MemberSchema):
        self.name = member_schema.name
        self.share = member_schema.share
        self.address = member_schema.address

    def set_id(self, id: int):
        self.id = id

    def schema(self) -> MemberSchema:
        return MemberSchema(name=self.name, share=self.share, address=self.address)

    def __str__(self):
        return 'member "%s" with share=%f%% and id=%i' % (self.name, self.share, self.id)


class Community:

    def __init__(self):
        self.next_id = 0
        self.members = dict()

    def add_member(self, member_schema: MemberSchema):
        self.members[self.next_id] = Member(member_schema)
        self.members[self.next_id].set_id(self.next_id)
        self.next_id += 1
        return self.next_id - 1

    def get_member(self, id: int) -> Member:
        if id in self.members:
            return self.members[id]
        else:
            return None

    def all_member_ids(self) -> Tuple[int]:
        return tuple(self.members.keys())


app = FastAPI(title="test application")

tags_metedata = [  # used for documentation of the web service (/docs)
    {
        'name': 'get member',
        'description': 'get a community member using its id'
    },{
        'name': 'create member',
        'description': 'create a new member in the community'
    },{
        'name': 'all members',
        'description': 'provide the list of all the recorded member ids'
    }
]


@app.get('/community/members', response_model=List[int], tags=['all members'])
async def get_all_members() -> List[int]:
    return community.all_member_ids()

@app.get('/community/get_member/{id}', response_model=MemberSchema, tags=['get member'])
async def get_member(id: int) -> MemberSchema:
    if id in community.members:
        return community.get_member(id).schema()
    else:
        raise HTTPException(status_code=404, detail="Member not found")

@app.post('/community/create_member', tags=['create member'])
async def add_member(member_schema: MemberSchema):
    return community.add_member(member_schema)


if __name__ == '__main__':
    community = Community()
    community.add_member(MemberSchema(name='1st member', share=0.12))
    community.add_member(MemberSchema(name='2nd member', share=0.34))
    uvicorn.run(app, host='127.0.0.1', port=8000)