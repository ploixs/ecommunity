import requests

server_port = 8000

response = requests.post(url="http://127.0.0.1:%i/community/create_member" % server_port, json = {'name': '3rd member', 'share': 0.19, 'address': 'Grenoble'})
print('status_code:', response.status_code)
print('content-type:', response.headers['content-type'])
print('text:', response.text)
