Unzip the project distrib.zip in your download folder.

Then, either you run the project from inside your download folder

or, 

you move to another location like in your Z:\
but, in the latter option, you may lack of disk space. In this case, you can put the 
database irise.sqlite3 in any location provided you update this location in the config.ini file.



Launch "Anaconda Powershell Prompt" application (if you use a Mac or Linux, open a terminal).

Go inside the unzipped folder:

cd <PATH_TO_DISTRIB_ROOT>

pip install -r requirements.txt

python -m notebook

It will open the default Internet browser. If it is Internet Explorer or Safari, close it and copy
 the URL http://localhost... appearing in the Powershell and paste it to Google Chrome or Firefox

Then open the relevant notebook (extension .ipynb)
