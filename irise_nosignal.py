from ecommunity import irise, scheduler
import datetime
import random


class PVplant(irise.PVplant):
    """
    A synchronized system representing a PV plant. Prediction of production is dependent on the weather but ignore nebulosity. AAdditional and random variation is introduced. The actual is the one calculated exactly with the weather and the nebulosity.
    """
    def __init__(self, simulator):
        super().__init__(simulator, name='PVplant:PV(88m2)', pv_efficiency=.13, pv_surface=88, slope=147, exposure=2, randomize_ratio=.2)
        self.predicted_powers = []

    def day_system_step_2(self, hour_slots: int, the_datetime: datetime.datetime):
        self.predicted_powers.extend([self.predicted_supplied_powers[k] for k in hour_slots])

    def hour_system_step_b(self, hour_slot: int, the_datetime: datetime.datetime):
        pass

    def hour_system_step_d(self, hour_slot: int, the_datetime: datetime.datetime):
        pass


class House(irise.House):
    """House corresponding to a house described in the IRISE database."""

    def __init__(self, simulator: scheduler.Simulator, name: str, irise_house: irise.IRISEhouse):
        """Initialize a house from the irise database and introduce the extracted house in the simulation.

        :param simulator: the simulator that's going to handle the simulation
        :type simulator: irise.IRISEsimulator
        :param name: name of the house with the group name first, in the format: "group_name:name of the house"
        :type name: str
        :param irise_house: data of a house extracted from IRISE
        :type irise_house: irise.IRISEhouse
        """
        super().__init__(simulator, name, irise_house)
        self.predicted_powers = []
        self.randomize_households = irise_house.randomize_households

    def day_system_step_2(self, hour_slots: int, the_datetime: datetime.datetime):
        self.predicted_powers.extend([self.predicted_consumed_powers[k] for k in hour_slots])

    def hour_system_step_b(self, hour_slot: int, the_datetime: datetime.datetime):
        """Model the household reaction to a red, white and green hour slot

        :param hour_slot: current hour slot index
        :type hour_slot: int
        :param the_datetime: see super-class
        :type the_datetime: datetime.datetime
        """
        presence = True
        if self.randomize_households:
            if the_datetime.weekday() < 5:
                if 8 <= the_datetime.hour <= 18:
                    presence = random.uniform(0,1) < .15
                else:
                    presence = random.uniform(0,1) < .95
            else:
                if 8 <= the_datetime.hour <= 18:
                    presence = random.uniform(0,1) < .60
                else:
                    presence = random.uniform(0,1) < .80
        hour_color = self.simulator.hour_colors[hour_slot]
        self.actual_consumed_powers.append(self.predicted_consumed_powers[hour_slot] * (presence * random.uniform(.8, 1.2) + (1 - presence)))

    def hour_system_step_d(self, hour_slot: int, the_datetime: datetime.datetime):
        """Model the household reaction to a red, white and green hour slot

        :param hour_slot: current hour slot index
        :type hour_slot: int
        :param the_datetime: see super-class
        :type the_datetime: datetime.datetime
        """
        pass


class IRISEsimulator(scheduler.Simulator):
    """Dedicated simulator for a community owning and PV plant and composed of several houses."""

    def __init__(self, from_local_stringdate: str, to_local_stringdate: str, local_timezone: str = "Europe/Paris", no_alert_threshold=500, randomize_households=False):
        super().__init__(from_local_stringdate, to_local_stringdate, local_timezone)
        """Initialize a community of houses with its related PV system

        :param from_local_stringdate: see super-class
        :type from_local_stringdate: str
        :param to_local_stringdate: see super-class
        :type to_local_stringdate: str
        :param local_timezone: see super-class
        :type local_timezone: str
        """
        self.pv_plant = PVplant(self)
        self.houses = list()
        for irise_house in irise.IRISE(simulator=self, randomize_households=randomize_households).irise_houses:
            self.houses.append(House(self, irise_house.name, irise_house=irise_house))
        print(self)
        self.no_alert_threshold = no_alert_threshold
        self.hour_colors = []

    def day_simulator_step_1(self, the_datetime: datetime, day_hour_slots, synchronized_systems_groups, init: bool):
        """Compute the color for each hour slot: RED suggests to reduce consumption, GREEN to increase and WHITE to do as usual.

        :param the_datetime: see super-class
        :type the_datetime: datetime.datetime
        :param day_hour_slots: see super-class
        :type day_hour_slots
        :param synchronized_systems_groups: the group with related synchronized systems (PV plant and houses)
        :type synchronized_systems_groups: dict[str, list[ecommunity.sync.AbstractSynchronizedSystem]]
        """
        pass

    def day_simulator_step_3(self, the_datetime: datetime, day_hour_slots, synchronized_systems_groups, init: bool):
        """Compute the color for each hour slot: RED suggests to reduce consumption, GREEN to increase and WHITE to do as usual.

        :param the_datetime: see super-class
        :type the_datetime: datetime.datetime
        :param day_hour_slots: see super-class
        :type day_hour_slots
        :param synchronized_systems_groups: the group with related synchronized systems (PV plant and houses)
        :type synchronized_systems_groups: dict[str, list[ecommunity.sync.AbstractSynchronizedSystem]]
        """
        pass

    def hour_simulator_step_a(self, the_datetime: datetime.datetime, the_hour_slot: int, synchronized_systems_groups):
        """Perform each hour computations: do nothing here

        :param the_datetime: the date with time (hour)
        :type the_datetime: datetime.datetime
        :param the_hour_slot: the current hour slot index
        :type the_hour_slot: int
        :param synchronized_systems_groups: the group with related synchronized systems (PV plant and houses)
        :type synchronized_systems_groups: dict[str, list[ecommunity.sync.AbstractSynchronizedSystem]]
        """
        hour_color = scheduler.COLOR.WHITE
        self.hour_colors.append(hour_color)

    def hour_simulator_step_c(self, the_datetime: datetime.datetime, the_hour_slot: int, synchronized_systems_groups):
        pass

    def hour_simulator_step_e(self, the_datetime: datetime.datetime, the_hour_slot: int, synchronized_systems_groups):
        pass

    def finalize(self, synchronized_systems_groups):
        """Compute indicators for the results and plot curves

        :param synchronized_systems_groups: Synchronized Systems gathered into groups in the simulation
        :type synchronized_systems_groups: dict[str, list[ecommunity.sync.AbstractSynchronizedSystem]]
        """
        pass


if __name__ == '__main__':
    irise_simulator = IRISEsimulator('16/02/2015', '01/02/2016',  no_alert_threshold=1000, randomize_households=True)
    # irise_simulator = IRISEsimulator('25/10/2015', '26/10/2015', no_alert_threshold=500)
    irise_simulator.run()
